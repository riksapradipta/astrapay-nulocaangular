import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

export interface responseProduct {
  idproduct: number;
  nameproduct: string;
  idcategory: number;
  // productList: string[]
}



@Injectable()
export class ProductService {
  BASE_API: string = environment.base_api

  constructor(
    private http: HttpClient
  ) { }


  getProductAll(): Observable<any>{
    const token = localStorage.getItem('token')
    const headers = new HttpHeaders({
      'Accept' : 'application/json',
      'Authorization' : 'Bearer ' + token
    })
    return this.http.get(`${this.BASE_API}/product/`, {headers: headers})
  }

  createProduct(payload: any){
    const token = localStorage.getItem('token')
    const headers = new HttpHeaders({
      'Accept' : 'application/json',
      'Authorization' : 'Bearer ' + token
    })
    return this.http.post(`${this.BASE_API}/product/`, payload, {headers: headers})
  }

  updateProduct(idproduct: number, payload: any){
    console.log("idproduct: ", idproduct)
    const token = localStorage.getItem('token')
    const headers = new HttpHeaders({
      'Accept' : 'application/json',
      'Authorization' : 'Bearer ' + token
    })
    return this.http.put(`${this.BASE_API}/product/${idproduct}`, payload , {headers: headers})
  }

  deleteProduct(idproduct: number) {
    const token = localStorage.getItem('token')
    const header = new HttpHeaders({
      'Authorization' : 'Bearer ' + token
    })

    return this.http.delete(`${this.BASE_API}/product/${idproduct}`, { headers: header })

  }

  //Categories
  getAllCategories(): Observable<any>{
    const token = localStorage.getItem('token')
    const headers = new HttpHeaders({
      'Accept' : 'application/json',
      'Authorization' : 'Bearer ' + token
    })
    return this.http.get(`${this.BASE_API}/category/`, {headers: headers})
  }

  getProductsInsideCategory(idcategory: number): Observable<any>{
    const token = localStorage.getItem('token')
    const headers = new HttpHeaders({
      'Accept' : 'application/json',
      'Authorization' : 'Bearer ' + token
    })
    return this.http.get(`${this.BASE_API}/category/${idcategory}`, { headers: headers })
  }

  createCategories(payload: any){
    const token = localStorage.getItem('token')
    const headers = new HttpHeaders({
      'Accept' : 'application/json',
      'Authorization' : 'Bearer ' + token
    })
    return this.http.post(`${this.BASE_API}/category/`, payload, {headers: headers})
  }

  deleteCategories(id: number){
    const token = localStorage.getItem('token')
    const headers = new HttpHeaders({
      'Accept' : 'application/json',
      'Authorization' : 'Bearer ' + token
    })
    return this.http.delete(`${this.BASE_API}/category/${id}`, {headers: headers})
  }

  updateCategories(idcategory: number ,payload: any){
    console.log("idcategory: ", idcategory)

    const token = localStorage.getItem('token')
    const headers = new HttpHeaders({
      'Accept' : 'application/json',
      'Authorization' : 'Bearer ' + token
    })
    return this.http.put(`${this.BASE_API}/category/${idcategory}`, payload , {headers: headers})
  }
}



