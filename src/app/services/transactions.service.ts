import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

// private int idtransaction;
// private long amounttransaction;
// private int idusertrans;
// private int idproducttrans;
// private int idproductdetailtrans;

export interface responseTransactions {
  idtransaction: number;
  amounttransaction: number;
  idproducttrans: number;
  idproductdetailtrans: number
}

@Injectable({
  providedIn: 'root'
})
export class TransactionsService {
  BASE_API: string = environment.base_api

  constructor(
    private http: HttpClient
  ) { }


  getAllTransaction(): Observable<any> {
    const token = localStorage.getItem('token')
    const header = new HttpHeaders({
      'Authorization' : 'Bearer ' + token
    })

    return this.http.get<any>(`${this.BASE_API}/payment/`, { headers: header })
  }

}
