import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

// "username": "riksapradipta123",
// "email": null,
// "phonenumber": null,
// "profilepict": null

export interface responseUser {
  iduser: number
  username: string;
  email: string;
  phonenumber: number;
  ewalletbalance: number

}

@Injectable()
export class UsersService {

  BASE_API: string = environment.base_api

  constructor(
    private http: HttpClient
  ) { }


  getUsersSubscriptions(iduser: number):Observable<any> {
    const token = localStorage.getItem('token')
    const header = new HttpHeaders({
      'Authorization' : 'Bearer ' + token
    })
    return this.http.get<responseUser>(`${this.BASE_API}/user/subscription/${iduser}`, { headers: header })
  }

  getUsersAll(): Observable<any> {
    const token = localStorage.getItem('token')
    const header = new HttpHeaders({
      'Authorization' : 'Bearer ' + token
    })

    return this.http.get<responseUser>(`${this.BASE_API}/user/`, { headers: header })
  }

  createUsers(payload: any) {
    const token = localStorage.getItem('token')
    const header = new HttpHeaders({
      'Authorization' : 'Bearer ' + token
    })

    return this.http.post(`${this.BASE_API}/user/`, payload, { headers: header } )
  }

  updateUsers(iduser: number, payload: any) {
    const token = localStorage.getItem('token')
    const header = new HttpHeaders({
      'Authorization' : 'Bearer ' + token,
      'Accept': 'Application/json'
    })

    return this.http.put(`${this.BASE_API}/user/${iduser}`, payload, { headers: header } )

  }

  deleteUsers(iduser: number) {
    const token = localStorage.getItem('token')
    const header = new HttpHeaders({
      'Authorization' : 'Bearer ' + token
    })

    return this.http.delete(`${this.BASE_API}/user/${iduser}`, { headers: header })

  }
}
