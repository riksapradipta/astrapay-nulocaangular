import { Component, Inject, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ManageusersSubscriptionsComponent } from '../../manageusers/manageusers-subscriptions/manageusers-subscriptions.component';



export interface CategoriesDetailsElement {
  idproduct : number
  nameproduct : string
  idcategory : number
  productpict : string
}


const ELEMENT_DATA: CategoriesDetailsElement[] = [];

@Component({
  selector: 'app-categoriesdetails',
  templateUrl: './categoriesdetails.component.html',
  styleUrls: ['./categoriesdetails.component.css']
})


export class CategoriesdetailsComponent implements OnInit {

  displayedColumns: string[] = ['Product ID', 'Product Name', 'Category ID', 'Product Pict'];
  dataSource = ELEMENT_DATA
  count: number

  constructor(
    public dialog: MatDialog,
    private productService: ProductService,

    @Inject(MAT_DIALOG_DATA) public idcategory: any
  ) { }

  // this.productForm = this.fb.group({
  //   email: [this.data ? this.data.email : '', [Validators.required]],
  //   ewalletbalance: [this.data ? this.data.ewalletbalance : 0],

  //   username: [this.data ? this.data.username : '', [Validators.required]],
  // })

  ngOnInit(): void {
    this.doGetProductsInsideCategory(this.idcategory)
  }

  doGetProductsInsideCategory(idcategory: any) {

    this.productService.getProductsInsideCategory(idcategory).subscribe(
      res =>{
        //this.dataSource=res.categoryList
        console.log(res);
        //this.openSubscription(res)
        this.dataSource = res.productList

        this.count = Object.keys(res.productList).length;
      }, err => {
        console.error(err)
      })

    // this.userService.getUsersAll().subscribe(
    //   res => {
    //     console.log(res);
    //     this.dataSource = res

    //     this.count = Object.keys(res).length;
    //     console.log(res)

    // }, err => {
    //     console.error(err)
    // })

  }

}
