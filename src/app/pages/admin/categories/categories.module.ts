import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoriesComponent } from './categories.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule,  } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule} from '@angular/material/dialog';
import { RouterModule } from '@angular/router';
import { ProductService } from 'src/app/services/product.service';
import { CategoriesinputComponent } from './categoriesinput/categoriesinput.component';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import { CategoriesdetailsComponent } from './categoriesdetails/categoriesdetails.component';
@NgModule({
  declarations: [
    CategoriesComponent,
    CategoriesinputComponent,
    CategoriesdetailsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(
      [
        {
          path: '',
          component: CategoriesComponent
        }
      ]),
      FormsModule,
      MatIconModule,
      MatCardModule,
      ReactiveFormsModule,
      MatTableModule,
      MatButtonModule,
      MatDialogModule
  ],
  providers: [ ProductService ],
  entryComponents: [ CategoriesinputComponent ]
})
export class CategoriesModule { }
