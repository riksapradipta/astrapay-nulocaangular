import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categoriesinput',
  templateUrl: './categoriesinput.component.html',
  styleUrls: ['./categoriesinput.component.css']
})
export class CategoriesinputComponent implements OnInit {

  // categoryForm: FormGroup

  // constructor(
  //   private fb: FormBuilder,
  //   private dialogRef: MatDialogRef<CategoriesinputComponent>,
  //   @Inject(MAT_DIALOG_DATA) public data: any
  // ) {
  //   console.log(this.data);
  //   this.initForm()
  // }

  // ngOnInit(): void {
  // }

  // initForm() {
  //   this.categoryForm = this.fb.group({
  //     namecategory: [this.data ? this.data.namecategory : '', [Validators.required]],
  //   })
  // }


  // closeDialog() {

  //   if (this.data?.idproduct) {
  //     this.dialogRef.close({ action: 'update', data: this.categoryForm.value, namecategory: this.data.namecategory })
  //   } else {
  //     this.dialogRef.close({ action: 'add', data: this.categoryForm.value })
  //   }

  // }
  categoryForm: FormGroup

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<CategoriesinputComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    console.log(this.data);
    this.initForm()
  }

  ngOnInit(): void {
  }

  initForm() {
    this.categoryForm = this.fb.group({
      namecategory: [this.data ? this.data.namecategory : '', [Validators.required]],
    })
  }


  closeDialog() {

    if (this.data?.idcategory) {
      this.dialogRef.close({ action: 'update', data: this.categoryForm.value, idcategory: this.data.idcategory })
    } else {
      this.dialogRef.close({ action: 'add', data: this.categoryForm.value })
    }

  }

}
