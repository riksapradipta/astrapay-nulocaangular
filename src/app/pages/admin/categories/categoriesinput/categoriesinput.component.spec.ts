import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriesinputComponent } from './categoriesinput.component';

describe('CategoriesinputComponent', () => {
  let component: CategoriesinputComponent;
  let fixture: ComponentFixture<CategoriesinputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoriesinputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriesinputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
