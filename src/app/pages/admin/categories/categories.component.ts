import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { ProductService } from 'src/app/services/product.service';
import { CategoriesinputComponent } from './categoriesinput/categoriesinput.component';
import Swal from 'sweetalert2';
import { CategoriesdetailsComponent } from './categoriesdetails/categoriesdetails.component';

export interface CategoriesElement {
}

const ELEMENT_DATA: CategoriesElement[] = [];



@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})


export class CategoriesComponent implements OnInit {

    // idcategory!: number

    displayedColumns: string[] = ['Category Name', 'action'];
    dataSource = ELEMENT_DATA;

    constructor(
      // private authService: AuthService,
      private router: Router,
      private productService: ProductService,
      public dialog: MatDialog
    ) { }

    ngOnInit(): void {
      this.doGetCategories()
    }

    async openDetails(datas?: any): Promise<any> {
      let dialogRef = await this.dialog.open(CategoriesdetailsComponent, {
        width: '750px',
        data: datas ? datas : {},
      });

      dialogRef.afterClosed().subscribe( res => {
        console.log('check ',res)
        if(res?.action === 'add') {
            this.doAddCategories(res.data)
          } else if ( res?.action === 'update') {
             this.doEditCategories(res.idcategory, res.data)
          //action untuk update
        } else {
          return null
        }
        }, err => {
          console.error(err)
        })
      }

    async openDialog(datas?: any): Promise<any> {
      let dialogRef = await this.dialog.open(CategoriesinputComponent, {
        width: '750px',
        data: datas ? datas : {},
      });

      dialogRef.afterClosed().subscribe( res => {
        console.log('check ',res)
        if(res?.action === 'add') {
            this.doAddCategories(res.data)
          } else if ( res?.action === 'update') {
             this.doEditCategories(res.idcategory, res.data)
          //action untuk update
        } else {
          return null
        }
        }, err => {
          console.error(err)
        })
      }

    doGetCategories(){
      this.productService.getAllCategories().subscribe(
      res =>{
        this.dataSource=res.categoryList
        console.log();
      }, err => {
        console.error(err)
      })
    }

    doAddCategories(payload: any){
      this.productService.createCategories(payload).subscribe(
      res =>{
        //this.dataSource=res.categoryList
        console.log(res);
        this.doGetCategories();

      }, err => {
        console.error(err)
      })
    }


    doEditCategories(idcategory: number, payload: any){
      this.productService.updateCategories(idcategory, payload).subscribe(
      res =>{
        //this.dataSource=res.categoryList
        console.log(res);
        this.doGetCategories()
      }, err => {
        console.error(err)
      })
    }

    doDeleteCategories(idcategory: number) {
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })

      swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
        }).then((result) => {
        if (result.isConfirmed) {
          this.productService.deleteCategories(idcategory). subscribe(
            res => {
              console.log(res);
              swalWithBootstrapButtons.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )
            this.doGetCategories();
        }, err => {
          console.error(err)
          swalWithBootstrapButtons.fire(
            'Failed Deleted',
            'Your file has fail deleted.',
            'error'
          )
        })
          } else if (
          /* Read more about handling dismissals below */
           result.dismiss === Swal.DismissReason.cancel
          ) {
        swalWithBootstrapButtons.fire(
            'Cancelled',
            'Your imaginary file is safe :)',
            'error'
        )
      }
    })
  }

}
