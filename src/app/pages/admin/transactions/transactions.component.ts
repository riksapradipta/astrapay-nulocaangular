import { Component, OnInit } from '@angular/core';
import Chart from 'chart.js/auto';
import { TransactionsInputComponent } from './transactions-input/transactions-input.component';
import { MatDialog } from '@angular/material/dialog';
import { TransactionsService } from 'src/app/services/transactions.service';



export interface TransactionElement {
  idtransaction: number
  amounttransaction: number
  idusertrans: number
  idproducttrans: number
  idproductdetailtrans: number
}

const ELEMENT_DATA: TransactionElement[] = []

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {

  displayedColumns: string[] = ['Transaction ID', 'Transaction Amount'];
  dataSource = ELEMENT_DATA
  count: number

  constructor(
    public dialog: MatDialog,
    private transactionService: TransactionsService,
  ) { }

  ngOnInit(): void {
    this.doGetTransaction();
  }


  async openDialog(datas?: any): Promise<any> {
    let dialogRef = await this.dialog.open(TransactionsInputComponent, {
      width: '750px',
      data: datas ? datas : {},
    });

    // dialogRef.afterClosed().subscribe( res => {
    //   console.log('check ',res)
    //   if(res?.action === 'add') {
    //     this.doAddUser(res.data)
    //   } else if ( res?.action === 'update') {
    //     //action untuk update
    //   } else {
    //     return null
    //   }
    // }, err => {
    //   console.error(err)
    // })

  }

  doGetTransaction() {
    this.transactionService.getAllTransaction().subscribe(
      res => {
        console.log(res);
        this.dataSource = res

        this.count = Object.keys(res).length;
        console.log(res)

    }, err => {
        console.error(err)
    })

  }

}
