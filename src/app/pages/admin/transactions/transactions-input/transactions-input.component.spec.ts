import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionsInputComponent } from './transactions-input.component';

describe('TransactionsInputComponent', () => {
  let component: TransactionsInputComponent;
  let fixture: ComponentFixture<TransactionsInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransactionsInputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionsInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
