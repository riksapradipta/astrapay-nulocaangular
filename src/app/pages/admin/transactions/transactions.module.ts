import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransactionsComponent } from './transactions.component';
import { TransactionsInputComponent } from './transactions-input/transactions-input.component';
import { RouterModule } from '@angular/router';
import { TransactionsService } from 'src/app/services/transactions.service';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
@NgModule({
  declarations: [
    TransactionsComponent,
    TransactionsInputComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatButtonModule,
    MatDialogModule,
    MatTableModule,
    RouterModule.forChild([
      {
        path: '',
        component: TransactionsComponent
      }
    ])
  ],
  providers: [ TransactionsService ],
  entryComponents: [ TransactionsInputComponent ]
})
export class TransactionsModule { }
