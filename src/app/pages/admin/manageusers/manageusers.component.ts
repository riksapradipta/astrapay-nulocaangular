import { Component, OnInit } from '@angular/core';
import { responseUser, UsersService } from 'src/app/services/users.service';
import { MatDialog } from '@angular/material/dialog';
import { DashboardInputComponent } from '../dashboard/dashboard-input/dashboard-input.component';
import { responseProduct } from 'src/app/services/product.service';
import { ManageusersInputComponent } from './manageusers-input/manageusers-input.component';
import Swal from 'sweetalert2'
import { ManageusersSubscriptionsComponent } from './manageusers-subscriptions/manageusers-subscriptions.component';


export interface UserElement {
  iduser : number
  username : string
  email : string
  phonenumber : number
  ewalletbalance : number
}


const ELEMENT_DATA: UserElement[] = [];

@Component({
  selector: 'app-manageusers',
  templateUrl: './manageusers.component.html',
  styleUrls: ['./manageusers.component.css']
})

export class ManageusersComponent implements OnInit {

  displayedColumns: string[] = ['User ID', 'Username', 'Email', 'Balance', 'Action'];
  dataSource = ELEMENT_DATA
  count: number


  constructor(
    public dialog: MatDialog,
    private userService: UsersService,
  ) { }

  ngOnInit(): void {
    this.doGetUser();
  }

  async openDialog(datas?: any): Promise<any> {
    let dialogRef = await this.dialog.open(ManageusersInputComponent, {
      width: '750px',
      data: datas ? datas : {},
    });

    dialogRef.afterClosed().subscribe( res => {
      console.log('check ',res)
      if(res?.action === 'add') {
        this.doAddUser(res.data)
      } else if ( res?.action === 'update') {
        //action untuk update
      } else {
        return null
      }
    }, err => {
      console.error(err)
    })

  }

  async openSubscription(iduser: number): Promise<any> {
    let dialogRef = await this.dialog.open(ManageusersSubscriptionsComponent, {
      width: '750px',
      data: iduser ? iduser : {},
    });

    dialogRef.afterClosed().subscribe( res => {
      console.log('check ',res)
      // if(res?.action === 'add') {
      //   this.doAddUser(res.data)
      // } else if ( res?.action === 'update') {
      //   //action untuk update
      // } else {
      //   return null
      // }
    }, err => {
      console.error(err)
    })

  }

  doGetUser() {
    this.userService.getUsersAll().subscribe(
      res => {
        console.log(res);
        this.dataSource = res

        this.count = Object.keys(res).length;
        console.log(res)

    }, err => {
        console.error(err)
    })

  }

  doUpdate(iduser: number,  payload: any) {
    this.userService.updateUsers(iduser, payload).subscribe(
      res => {
        console.log(res)
        this.doGetUser();
        // this.dataSource=res.consultantList
      },
      err => {
        console.log(err)
      }
    )
}

  doAddUser(payload: any) {
    this.userService.createUsers(payload).subscribe(
      res => {
        console.log(res);
        this.doGetUser();
    }, err => {
        console.error(err)
    })

  }

  // doGetSubscriptions(iduser: number){
  //   console.log("idproduct:", iduser)

  //   this.userService.getUsersSubscriptions(iduser).subscribe(
  //   res =>{
  //     //this.dataSource=res.categoryList
  //     console.log(res);
  //     this.openSubscription(res)
  //   }, err => {
  //     console.error(err)
  //   })

  // }

  doDeleteUser(iduser: number) {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
      }).then((result) => {
      if (result.isConfirmed) {
        this.userService.deleteUsers(iduser). subscribe(
          res => {
            console.log(res);
            swalWithBootstrapButtons.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
          this.doGetUser();
      }, err => {
        console.error(err)
        swalWithBootstrapButtons.fire(
          'Failed Deleted',
          'Your file has fail deleted.',
          'error'
        )
      })
    } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelled',
          'Your imaginary file is safe :)',
          'error'
        )
      }
    })
  }

}
