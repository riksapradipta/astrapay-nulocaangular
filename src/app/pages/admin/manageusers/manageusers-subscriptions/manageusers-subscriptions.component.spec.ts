import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageusersSubscriptionsComponent } from './manageusers-subscriptions.component';

describe('ManageusersSubscriptionsComponent', () => {
  let component: ManageusersSubscriptionsComponent;
  let fixture: ComponentFixture<ManageusersSubscriptionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageusersSubscriptionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageusersSubscriptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
