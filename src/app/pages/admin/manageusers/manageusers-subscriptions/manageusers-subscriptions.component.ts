import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UsersService } from 'src/app/services/users.service';

export interface SubscriptionsElement {
  idproduct : number
  nameproduct : string
  idcategory : number
  productpict : string
}


const ELEMENT_DATA: SubscriptionsElement[] = [];

@Component({
  selector: 'app-manageusers-subscriptions',
  templateUrl: './manageusers-subscriptions.component.html',
  styleUrls: ['./manageusers-subscriptions.component.css']
})
export class ManageusersSubscriptionsComponent implements OnInit {

  displayedColumns: string[] = ['Product ID', 'Product Name', 'Category ID', 'Product Pict'];
  dataSource = ELEMENT_DATA
  count: number

  constructor(
    public dialog: MatDialog,
    private userService: UsersService,
    @Inject(MAT_DIALOG_DATA) public iduser: any
  ) { }

  // this.productForm = this.fb.group({
  //   email: [this.data ? this.data.email : '', [Validators.required]],
  //   ewalletbalance: [this.data ? this.data.ewalletbalance : 0],

  //   username: [this.data ? this.data.username : '', [Validators.required]],
  // })

  ngOnInit(): void {
    this.doGetUserSubscriptions(this.iduser)
  }

  doGetUserSubscriptions(iduser: any) {

    this.userService.getUsersSubscriptions(iduser).subscribe(
      res =>{
        //this.dataSource=res.categoryList
        console.log(res);
        //this.openSubscription(res)
        this.dataSource = res.mySubscriptions

        this.count = Object.keys(res.mySubscriptions).length;
      }, err => {
        console.error(err)
      })


    // this.userService.getUsersAll().subscribe(
    //   res => {
    //     console.log(res);
    //     this.dataSource = res

    //     this.count = Object.keys(res).length;
    //     console.log(res)

    // }, err => {
    //     console.error(err)
    // })

  }

}
