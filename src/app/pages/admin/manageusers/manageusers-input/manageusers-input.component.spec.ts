import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageusersInputComponent } from './manageusers-input.component';

describe('ManageusersInputComponent', () => {
  let component: ManageusersInputComponent;
  let fixture: ComponentFixture<ManageusersInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageusersInputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageusersInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
