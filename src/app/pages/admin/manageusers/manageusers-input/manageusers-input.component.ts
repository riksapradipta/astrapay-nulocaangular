import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


@Component({
  selector: 'app-manageusers-input',
  templateUrl: './manageusers-input.component.html',
  styleUrls: ['./manageusers-input.component.css']
})
export class ManageusersInputComponent implements OnInit {

  productForm: FormGroup

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<ManageusersInputComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any

  )

  {
    console.log(this.data);
    this.initForm()
  }

  ngOnInit(): void {
  }

  initForm() {
    this.productForm = this.fb.group({
      email: [this.data ? this.data.email : '', [Validators.required]],
      ewalletbalance: [this.data ? this.data.ewalletbalance : 0],

      username: [this.data ? this.data.username : '', [Validators.required]],
    })
  }


  closeDialog() {

    if (this.data?.iduser) {
      this.dialogRef.close({ action: 'update', data: this.productForm.value, email: this.data.email, username: this.data.username, ewalletbalance: this.data.ewalletbalance })
    } else {
      this.dialogRef.close({ action: 'add', data: this.productForm.value })

    }

  }
}
