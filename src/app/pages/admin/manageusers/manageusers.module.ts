import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageusersComponent } from './manageusers.component';
import { RouterModule } from '@angular/router';
import { MatTableModule } from '@angular/material/table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { ManageusersInputComponent } from './manageusers-input/manageusers-input.component';
import { UsersService } from 'src/app/services/users.service';
import { ManageusersSubscriptionsComponent } from './manageusers-subscriptions/manageusers-subscriptions.component';



@NgModule({
  declarations: [ ManageusersComponent, ManageusersInputComponent, ManageusersSubscriptionsComponent ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatButtonModule,
    MatDialogModule,
    MatTableModule,
    RouterModule.forChild([
      {
        path: '',
        component: ManageusersComponent
      }
    ])
  ],
  providers: [UsersService],
  entryComponents: [ManageusersInputComponent]
})
export class ManageusersModule { }
