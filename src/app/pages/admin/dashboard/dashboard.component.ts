import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DashboardInputComponent } from './dashboard-input/dashboard-input.component';
import { ProductService } from 'src/app/services/product.service';
import Swal from 'sweetalert2'
import {MatPaginator} from '@angular/material/paginator';

import Chart from 'chart.js/auto';

export interface ProductElement {
  idproduct: number
  nameproduct: string
  idcategory: number
}

const ELEMENT_DATA: ProductElement[] = [

];



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']

})


export class DashboardComponent implements OnInit {

  displayedColumns: string[] = ['Product Name', 'Category ID', 'action'];
  dataSource = ELEMENT_DATA;
  count: number



  labelsNameProduct: string[]
  dataPriceProduct: number[]

  constructor(
    public dialog: MatDialog,
    private productService: ProductService,
  ) { }

  ngOnInit(): void {
    this.doGetProduct();
  }

  InitChart() {
    const ctx : any = document.getElementById('myChart');
    const myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: this.labelsNameProduct,
        datasets: [{
            label: 'Hasil Penjualan',
            data: this.dataPriceProduct,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});
return Chart

  }

  async openDialog(datas?: any): Promise<any> {
    let dialogRef = await this.dialog.open(DashboardInputComponent, {
      width: '750px',
      data: datas ? datas : {},
    });

    dialogRef.afterClosed().subscribe( res => {
      console.log('check ',res)
      if(res?.action === 'add') {
          this.doAddProduct(res.data)
        } else if ( res?.action === 'update') {
           this.doEditProduct(res.idproduct, res.data)
        //action untuk update
      } else {
        return null
      }
      }, err => {
        console.error(err)
      })
    }

    doEditProduct(idproduct: number, payload:any){
      console.log("idproduct:", idproduct)

      this.productService.updateProduct(idproduct, payload).subscribe(
      res =>{
        //this.dataSource=res.categoryList
        console.log(res);
        this.doGetProduct()
      }, err => {
        console.error(err)
      })

    }

  //   doUpdateProduct(idcategory: number, payload: any) {
  //     this.productService.updateProduct(idcategory, payload).subscribe(
  //       res => {
  //         console.log(res)
  //         this.doGetProduct();
  //         // this.dataSource=res.consultantList
  //       },
  //       err => {
  //         console.log(err)
  //       }
  //     )
  // }
  doGetProduct(){
    this.productService.getProductAll().subscribe(
    res =>{
      this.dataSource=res.productList
      console.log(res.productList);
        //       this.labelsNameProduct = []
          //       this.dataPriceProduct = []

          //       this.count = Object.keys(res).length;
  //       console.log(res)

  //       for(let data of this.dataSource) {
  //         this.labelsNameProduct.push(data.nameproduct)
  //         this.dataPriceProduct.push(data.idcategory)
  //       }

  //       this.InitChart()


    }, err => {
      console.error(err)
    })
  }

  // doGetProduct() {
  //   this.productService.getProductAll().subscribe(
  //     res => {
  //       this.dataSource = res.productList
  //       console.log(res);
  //       this.labelsNameProduct = []
  //       this.dataPriceProduct = []

  //       this.count = Object.keys(res).length;
  //       console.log(res)

  //       for(let data of this.dataSource) {
  //         this.labelsNameProduct.push(data.nameproduct)
  //         this.dataPriceProduct.push(data.idcategory)
  //       }

  //       this.InitChart()
  //   }, err => {
  //       console.error(err)
  //   })
  // }

  doAddProduct(payload: any) {
    this.productService.createProduct(payload).subscribe(
      res => {
        console.log(res);
        this.doGetProduct();
        window.location.reload();
    }, err => {
        console.error(err)
        Swal.fire({
          title: 'Error!',
          text: `${err.message}`,
          icon: 'error',
          confirmButtonText: 'Cool'
        })
    })
  }

  doDeleteProduct(idproduct: number) {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
      }).then((result) => {
      if (result.isConfirmed) {
        this.productService.deleteProduct(idproduct). subscribe(
          res => {
            console.log(res);
            swalWithBootstrapButtons.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
          this.doGetProduct();
      }, err => {
        console.error(err)
        swalWithBootstrapButtons.fire(
          'Failed Deleted',
          'Your file has fail deleted.',
          'error'
        )
      })
    } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelled',
          'Your imaginary file is safe :)',
          'error'
        )
      }
    })
  }

}


