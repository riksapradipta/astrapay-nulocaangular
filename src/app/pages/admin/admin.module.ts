import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AdminComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: AdminComponent,
        children: [
          {
            path: 'dashboard',
            loadChildren: () => import('./dashboard/dashboard.module').then( m => m.DashboardModule)
          },
          {
            path: 'categories',
            loadChildren: () => import('./categories/categories.module').then( m => m.CategoriesModule)
          },
          {
            path: 'transactions',
            loadChildren: () => import('./transactions/transactions.module').then( m => m.TransactionsModule)
          },
          {
              path: 'manageusers',
              loadChildren: () => import('./manageusers/manageusers.module').then( m => m.ManageusersModule)
          }
        ]
      }
    ])
  ]
})
export class AdminModule { }
